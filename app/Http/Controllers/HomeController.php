<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BankAccounts;
use App\Models\FinencialOrganization;
use Illuminate\Validation\Rule;
class HomeController extends Controller
{
    public function banklist()
    {
        $banks = FinencialOrganization::get();
        return response()->json($banks);
    }
    public function bankaccountlist()
    {
        $banks = BankAccounts::latest()->paginate(6);
        return response()->json($banks);
    }

    public function index()
    {
        return view('home');
    }
    public function test()
    {
        return response()->json("Success");
    }

    public function store(Request $request)
    {
        $request->validate([
            'account_name' => 'required|max:255',
            'account_no' => 'required|max:100',
            'brunch' => 'required|max:50',
            'swift_code' => 'required|max:100',
            'route_no' => 'required|max:100',
            'finencial_organization_id' =>['required',Rule::notIn(['0'])],
            'account_type' =>['required',Rule::notIn(['0'])],
        ]);

        $bankaccount = BankAccounts::create([
            'account_name' => $request->account_name,
            'account_no' => $request->account_no,
            'brunch' => $request->brunch,
            'swift_code' => $request->swift_code,
            'route_no' => $request->route_no,
            'finencial_organization_id' => $request->finencial_organization_id,
            'account_type' => $request->account_type,
            'store_id' => 0,
        ]);
     
        return response()->json('Success',200);
    }


    public function delete(Request $request, $id)
    {
        $bankaccount = BankAccounts::find($id)->delete();
        return response()->json('Success',200);
    }
    public function update(Request $request, $id)
    {
        $request->validate([
            'account_name' => 'required|max:255',
            'account_no' => 'required|max:100',
            'brunch' => 'required|max:50',
            'swift_code' => 'required|max:100',
            'route_no' => 'required|max:100',
            'finencial_organization_id' =>['required',Rule::notIn(['0'])],
            'account_type' =>['required',Rule::notIn(['0'])],
        ]);
        
        $bankaccount = BankAccounts::find($id);
            $bankaccount->account_name = $request->account_name;
            $bankaccount->account_no = $request->account_no;
            $bankaccount->brunch = $request->brunch;
            $bankaccount->swift_code = $request->swift_code;
            $bankaccount->route_no = $request->route_no;
            $bankaccount->finencial_organization_id = $request->finencial_organization_id;
            $bankaccount->account_type = $request->account_type;
            $bankaccount->store_id = 0;
            $bankaccount->save();
     
        return response()->json('Success',200);
    }
}
