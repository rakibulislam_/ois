<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
class BankAccounts extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $fillable = ['account_name',
    'account_no',
    'brunch',
    'swift_code',
    'route_no','finencial_organization_id','account_type','store_id'];
}
