<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class FinencialOrganization extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $fillable = ['name','address','short_name'];
}
