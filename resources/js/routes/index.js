import {
    Vue
} from "vue";

import BankAccount from "../components/BankAccount.vue";
import Home from "../components/Home.vue";

export const routes = [{
        path: '/bank-account',
        component: BankAccount,
        name: 'bankaccount'
    },
    {
        path: '/',
        component: Home,
        name:'home'
    }
]
