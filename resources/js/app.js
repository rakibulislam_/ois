require('./bootstrap');
window.Vue = require('vue').default;

import VueRouter from 'vue-router'
Vue.use(VueRouter)

import Vuelidate from 'vuelidate'
Vue.use(Vuelidate)

import {
    routes
} from "./routes/index";

const router = new VueRouter({
    routes // short for `routes: routes`
})

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('master', require('./components/master').default);
Vue.component('pagination', require('laravel-vue-pagination'));
const app = new Vue({
    el: '#app',
    router
});
