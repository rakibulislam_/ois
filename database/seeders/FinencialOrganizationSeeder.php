<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\FinencialOrganization;

class FinencialOrganizationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FinencialOrganization::create([
            'name' =>  'Bank Asia',
            'address' =>  'Dhaka',
            'short_name' =>  'BA',
        ]); 
        FinencialOrganization::create([
            'name' =>  'Dhaka Bank',
            'address' =>  'Dhaka',
            'short_name' =>  'DB',
        ]); 
        FinencialOrganization::create([
            'name' =>  'Brac Bank',
            'address' =>  'Dhaka',
            'short_name' =>  'BB',
        ]); 
        FinencialOrganization::create([
            'name' =>  'Southeast Bank',
            'address' =>  'Dhaka',
            'short_name' =>  'SB',
        ]); 
        FinencialOrganization::create([
            'name' =>  'Eastern Bank',
            'address' =>  'Dhaka',
            'short_name' =>  'EB',
        ]); 
        FinencialOrganization::create([
            'name' =>  'Padma Bank',
            'address' =>  'Dhaka',
            'short_name' =>  'PB',
        ]); 
        FinencialOrganization::create([
            'name' =>  'Jamuna Bank',
            'address' =>  'Dhaka',
            'short_name' =>  'JB',
        ]); 
        FinencialOrganization::create([
            'name' =>  'Cylon Bank',
            'address' =>  'Dhaka',
            'short_name' =>  'CB',
        ]); 
        FinencialOrganization::create([
            'name' =>  'Chittagong Bank',
            'address' =>  'Dhaka',
            'short_name' =>  'CHB',
        ]); 
        FinencialOrganization::create([
            'name' =>  'Khulna Bank',
            'address' =>  'Dhaka',
            'short_name' =>  'KB',
        ]); 
    }
}
// php artisan make:seeder ProductSeeder
// php artisan db:seed --class=ProductSeeder 