<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\BankAccounts;
class BankAccountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BankAccounts::create([
            'account_name' =>  'Account 1',
            'finencial_organization_id' =>  1,
            'store_id' =>  0,
            'account_no' =>  '123456789',
            'brunch' =>  'Dhaka',
            'account_type' =>  1,
            'swift_code' =>  '123456789',
            'route_no' =>  '123',
        ]);
        BankAccounts::create([
            'account_name' =>  'Account 1',
            'finencial_organization_id' =>  1,
            'store_id' =>  0,
            'account_no' =>  '123456789',
            'brunch' =>  'Dhaka',
            'account_type' =>  1,
            'swift_code' =>  '123456789',
            'route_no' =>  '123',
        ]);
        BankAccounts::create([
            'account_name' =>  'Account 1',
            'finencial_organization_id' =>  1,
            'store_id' =>  0,
            'account_no' =>  '123456789',
            'brunch' =>  'Dhaka',
            'account_type' =>  1,
            'swift_code' =>  '123456789',
            'route_no' =>  '123',
        ]);
        BankAccounts::create([
            'account_name' =>  'Account 1',
            'finencial_organization_id' =>  1,
            'store_id' =>  0,
            'account_no' =>  '123456789',
            'brunch' =>  'Dhaka',
            'account_type' =>  1,
            'swift_code' =>  '123456789',
            'route_no' =>  '123',
        ]);
        BankAccounts::create([
            'account_name' =>  'Account 1',
            'finencial_organization_id' =>  1,
            'store_id' =>  0,
            'account_no' =>  '123456789',
            'brunch' =>  'Dhaka',
            'account_type' =>  1,
            'swift_code' =>  '123456789',
            'route_no' =>  '123',
        ]);
        BankAccounts::create([
            'account_name' =>  'Account 1',
            'finencial_organization_id' =>  1,
            'store_id' =>  0,
            'account_no' =>  '123456789',
            'brunch' =>  'Dhaka',
            'account_type' =>  1,
            'swift_code' =>  '123456789',
            'route_no' =>  '123',
        ]);
    }
}
