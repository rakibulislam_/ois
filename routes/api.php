<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
// api/test
Route::get('/banklist', [App\Http\Controllers\HomeController::class, 'banklist']);
Route::get('/bankaccountlist', [App\Http\Controllers\HomeController::class, 'bankaccountlist']);
Route::get('/test', [App\Http\Controllers\HomeController::class, 'test']);
Route::post('/store', [App\Http\Controllers\HomeController::class, 'store']);
Route::post('/delete/{id}', [App\Http\Controllers\HomeController::class, 'delete']);
Route::post('/update/{id}', [App\Http\Controllers\HomeController::class, 'update']);